extends Area2D



func _on_BulletBoostMessage_body_entered(body):
	var button
	if get_tree().get_nodes_in_group("main").size() > 0:
		var main = get_tree().get_nodes_in_group("main")[0]
		if main.is_using_keyboard():
			button = main.control_handler.get_keyboard_key_name("ui_bullet_boost")
		elif main.is_using_controller():
			button = main.control_handler.get_controller_button_name("ui_bullet_boost", main.controller_layout)
	else:
		button = "F"
	
	if body.is_in_group("player") and $Timer.is_stopped():
		body.write(button + " while holding bullet: boost to bullet", 4)
		$Timer.start()